# Ansible in docker lab

Objective is to create Ansible in Docker setup 

### Directory Structure
```
├── Docker
│   ├── Ansible_Control_node
│   │   └── Dockerfile      
│   └── Target_Server
│       └── Dockerfile      
├── docker-compose.yml      
├── hosts            
└── playbook.yml
```

Host file looks like below 
```
[target]
server01
server02
```

Playbook simply creates file on the target servers 
```
- hosts: target
  tasks:
  - name: "Create new file devopsroles.txt"
    shell: |
        touch sbdevopsx.txt
```

Bring up the Docker containers - 
```
# docker-compose up -d
# docker ps 
# docker exec -it ansible /bin/bash
```

SSH connection without password from Ansible container to Two target container.
```
# ssh server01
exit
# ssh server02
exit
```

Run the Ansible command from Ansible container 
```
# ansible-inventory -i inventory --list
# ansible all -i inventory -m ping

# ansible-playbook -i hosts playbook.yml
```
